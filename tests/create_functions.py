def create_car(client):
    response = client.post("/cars/", json={"name": "BMW X5", "price": 3999999.99})
    assert response.status_code == 200, response.json()
    id_cinema = response.json()["id"]
    return id_cinema
