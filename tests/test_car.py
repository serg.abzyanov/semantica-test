import pytest

from fastapi.testclient import TestClient
from src.main import app
from .create_functions import create_car


def test_empty_list():
    with TestClient(app) as client:
        response = client.get("/cars/")
        assert response.status_code == 200
        assert response.json() == []


def test_create():
    with TestClient(app) as client:
        car_data = {"name": "BMW X5", "price": 3999999.99}
        # Check car create
        response = client.post("/cars/", json=car_data)
        assert response.status_code == 200, response.json()
        car_data["id"] = response.json()["id"]
        assert response.json() == car_data


def test_list():
    with TestClient(app) as client:
        # Create new car
        id_car = create_car(client)
        # Check new car in car list
        response = client.get("/cars/")
        assert response.status_code == 200
        assert response.json() == [
            {"id": id_car, "name": "BMW X5", "price": 3999999.99}
        ]


def test_get():
    with TestClient(app) as client:
        # Create
        id_car = create_car(client)
        # Check new car by id
        response = client.get(f"/cars/{id_car}")
        assert response.status_code == 200
        assert response.json() == {"id": id_car, "name": "BMW X5", "price": 3999999.99}


def test_get_not_found():
    with TestClient(app) as client:
        # Check new car by id
        response = client.get(f"/cars/{99}")
        assert response.status_code == 404


@pytest.mark.parametrize("key,value", [("name", "BMW X5"), ("price", 3999999.99)])
def test_update(key, value):
    with TestClient(app) as client:
        # Create
        id_car = create_car(client)
        response = client.patch(f"/cars/{id_car}", json={key: value})
        assert response.status_code == 200
        # Check new car by id
        response = client.get(f"/cars/{id_car}")
        assert response.status_code == 200
        expected = {"id": id_car, "name": "BMW X5", "price": 3999999.99}
        expected.update({key: value})
        assert response.json() == expected


def test_update_not_found():
    with TestClient(app) as client:
        response = client.patch(
            f"/cars/{99}", json={"name": "BMW X5", "price": 3999999.99}
        )
        assert response.status_code == 404


def test_delete():
    with TestClient(app) as client:
        # Create
        id_car = create_car(client)
        # Delete booking
        response = client.delete(f"/cars/{id_car}")
        assert response.status_code == 200
        response = client.get(f"/cars/{id_car}")
        assert response.status_code == 404


def test_delete_not_found():
    with TestClient(app) as client:
        response = client.delete(f"/cars/{99}")
        assert response.status_code == 404
