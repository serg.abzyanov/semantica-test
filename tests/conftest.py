import pytest

from src.db import Car, init_db, close_db


@pytest.fixture(scope="function", autouse=True)
async def clean_db_records():
    await init_db()
    await Car.delete.gino.status()
    await close_db()
    yield
