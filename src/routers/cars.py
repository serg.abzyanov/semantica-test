from typing import List

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
from src.db import db, Car
from ..utils.middlewares import ContextIncludedRoute

router = APIRouter(route_class=ContextIncludedRoute)


class CarIn(BaseModel):
    name: str
    price: float


class CarOut(BaseModel):
    id: int
    name: str
    price: float

    @classmethod
    def from_model(cls, m: Car):
        return cls(id=m.id, name=m.name, price=m.price)


class CarInUpdate(BaseModel):
    name: str = None
    price: float = None


@router.get("/", response_model=List[CarOut])
async def get_car_list():
    async with db.transaction():
        car_list = await Car.query.order_by(Car.id).gino.all()
        car_list = [CarOut.from_model(car) for car in car_list]
        return car_list


@router.post("/", response_model=CarOut)
async def create_car(car_in: CarIn):
    car = await Car.create(
        name=car_in.name,
        price=car_in.price,
    )
    return CarOut.from_model(car)


@router.get("/{id_car}", response_model=CarOut)
async def get_car(id_car: int):
    car = await Car.query.where(Car.id == id_car).gino.first()
    if not car:
        raise HTTPException(status_code=404, detail="Car not found")
    return CarOut.from_model(car)


@router.patch("/{id_car}")
async def update_car(id_car: int, car_in: CarInUpdate):
    car = await Car.query.where(Car.id == id_car).gino.first()
    if not car:
        raise HTTPException(status_code=404, detail="Car not found")
    if car_in.name:
        await car.update(name=car_in.name).apply()
    if car_in.price:
        await car.update(price=car_in.price).apply()
    return f"Car {id_car} updated. New data {CarOut.from_model(car)}"


@router.delete("/{id_car}")
async def delete_car(id_car: int):
    car = await Car.query.where(Car.id == id_car).gino.first()
    if not car:
        raise HTTPException(status_code=404, detail="Car not found")
    await Car.delete.where(Car.id == id_car).gino.status()
    return f"Car {id_car} successfully deleted"
