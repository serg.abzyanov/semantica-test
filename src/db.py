from gino import Gino
from .settings import DB_HOST, DB_PORT, DB_USER, DB_PASSWORD, DB_NAME

db = Gino()


class Car(db.Model):
    __tablename__ = "car"

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.Unicode(), nullable=False)
    price = db.Column(db.Float(), nullable=False)


async def init_db():
    await db.set_bind(
        f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
    )
    # Create tables
    await db.gino.create_all()


async def close_db():
    engine, db.bind = db.bind, None
    await engine.close()
