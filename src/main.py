from fastapi import FastAPI
from .routers import cars

from .db import init_db, close_db

app = FastAPI()


@app.on_event("startup")
async def startup():
    await init_db()


@app.on_event("shutdown")
async def shutdown_event():
    await close_db()


app.include_router(cars.router, prefix="/cars")
