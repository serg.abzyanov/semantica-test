import os

DB_USER = os.getenv("DB_USER", "cars")
DB_PASSWORD = os.getenv("DB_PASSWORD", "cars")
DB_NAME = os.getenv("DB_NAME", "cars")
DB_HOST = os.getenv("DB_HOST", "localhost")
DB_PORT = os.getenv("DB_PORT", "5432")
