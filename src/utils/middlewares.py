from typing import Callable

from fastapi import Request, Response
from fastapi.routing import APIRoute


class ContextIncludedRoute(APIRoute):
    def get_route_handler(self) -> Callable:
        original_route_handler = super().get_route_handler()

        async def custom_route_handler(request: Request) -> Response:
            response: Response = await original_route_handler(request)

            if await request.body():
                request_output = f"Request body: {await request.body()}"
                print(request_output)
            else:
                print("Request body: empty")

            if response.body:
                response_output = f"Response body: {response.body}"
                print(response_output)
            else:
                print("Response body: empty")

            return response

        return custom_route_handler
