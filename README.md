# Docker - build and run

```shell script
$ docker-compose build
$ docker-compose run --service-ports cars-service app
```

# Docker - run tests

```shell script
$  docker-compose run cars-service pytest -vv
```

# Docker - stop/shutdown containers

```shell script
$ docker-compose down
```